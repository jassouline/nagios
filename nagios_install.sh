#Check if the user is root

if [ "$UID" -ne "0" ]
then
   echo "You have to launch this script as root"
   exit 1
fi

#Log File for the script
touch /tmp/install.log

echo "Updating of aptitude tool"
apt-get update >> /tmp/install.log


echo "Packets installation"
echo "Packet installation 1%"
apt-get install -y gcc >> /tmp/install.log
echo "Packet installation 10%"
apt-get install -y make >> /tmp/install.log
echo "Packet installation 30%"
apt-get install -y binutils cpp libpq-dev libmysqlclient-dev >> /tmp/install.log
echo "Packet installation 50%"
apt-get install -y libssl1.0.0 libssl-dev pkg-config libgd2-xpm-dev libgd-tools >> /tmp/install.log
echo "Packet installation 70%"
apt-get install -y perl libperl-dev libnet-snmp-perl snmp >> /tmp/install.log
echo "Packet installation 90%"
apt-get install -y apache2 libapache2-mod-php5 git >> /tmp/install.log


echo "Creating Directory /usr/src/nagios4"
mkdir /usr/src/nagios4
cd /usr/src/nagios4

echo "Downloading of nagios and nagios-plugins"
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.3.1.tar.gz >> /tmp/install.log
wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz >> /tmp/install.log
tar -xzf nagios-4.3.1.tar.gz >> /tmp/install.log
tar -xzf nagios-plugins-2.2.1.tar.gz >> /tmp/install.log

echo "Groups and users creation"
groupadd nagios
groupadd nagioscmd
useradd -g nagios -G nagioscmd -d /opt/nagios nagios
usermod -a -G nagioscmd www-data
mkdir -p /opt/nagios /etc/nagios /var/nagios
chown nagios:nagios /opt/nagios /etc/nagios /var/nagios


#Nagios4 Installation
echo "*********************"
echo "Nagios 4 installation"
echo "*********************"
cd /usr/src/nagios4/nagios-4.3.1
sh configure --prefix=/opt/nagios --sysconfdir=/etc/nagios --localstatedir=/var/nagios --libexecdir=/opt/nagios/plugins --with-command-group=nagioscmd --with-mail=/usr/sbin/sendmail
make all >> /tmp/install.log

make install >> /tmp/install.log
make install-commandmode >>/tmp/install.log
make install-config >> /tmp/install.log
make install-init >> /tmp/install.log
update-rc.d nagios defaults


echo "*****************************"
echo "Nagios 4 Plugins installation"
echo "*****************************"
cd /usr/src/nagios4/nagios-plugins-2.2.1
sh configure --prefix=/opt/nagios --sysconfdir=/etc/nagios --localstatedir=/var/nagios --libexecdir=/opt/nagios/plugins >> /tmp/install.log
make all >> /tmp/install.log
make install >> /tmp/install.log

echo "***********************"
echo "Starting Nagios service"
echo "***********************"
service nagios start


echo "**********************************"
echo "Settings parameters for nagios.cfg"
echo "**********************************"
cd /tmp
git clone https://jassouline@gitlab.com/jassouline/nagios.git >> /tmp/install.log
cd nagios
cp nagios.cfg /etc/nagios/nagios.cfg

cd /etc/nagios
mkdir hosts hostgroups services servicegroups contacts contactgroups timeperiods commands templates
cp objects/commands.cfg commands/default.cfg

echo "***********************************************"
echo "Settings parameters for a localhost nagios host"
echo "***********************************************"
cd /tmp/nagios
cp localhost.cfg /etc/nagios/hosts/localhost.cfg

echo "*********************************************"
echo "Settings parameters for the first web service"
echo "*********************************************"
cp localhost-www.cfg /etc/nagios/services/localhost-www.cfg

echo "*********************************************"
echo "Settings parameters for the first ssh service"
echo "*********************************************"
cp localhost-ssh.cfg /etc/nagios/services/localhost-ssh.cfg

echo "********************************************"
echo "Settings parameters for the timeperiods file"
echo "********************************************"
cp default.cfg /etc/nagios/timeperiods/default.cfg

echo "****************************************"
echo "Settings parameters for the contact file"
echo "****************************************"
cp default.cfg /etc/nagios/timeperiods/default.cfg
cp admins.cfg /etc/nagios/contactgroups/admins.cfg
cp jassouline.cfg /etc/nagios/contacts/jassouline.cfg
cp apache2/nagiosadmin.cfg /etc/nagios/contacts/nagiosadmin.cfg
cp apache2/nagios.conf  /etc/apache2/sites-available/nagios.conf

echo "*************************"
echo "Test for the installation"
echo "*************************"
/opt/nagios/bin/nagios -v /etc/nagios/nagios.cfg


echo "********************"
echo "Authentication Files"
echo "********************"

cp /dev/null /etc/nagios/htpasswd.groups
echo "Please enter the password you want for the nagiosadmin user"
htpasswd -c /etc/nagios/htpasswd.users nagiosadmin
chown root:nagioscmd /etc/nagios/htpasswd.*
chmod 0640 /etc/nagios/htpasswd.*


echo "************************"
echo "Web Interface Activation"
echo "************************"
ln -s /etc/apache2/sites-available/nagios.conf /etc/apache2/sites-enabled/nagios.conf
a2enmod authz_groupfile
a2enmod cgi
a2ensite nagios
service apache2 restart
service nagios restart
